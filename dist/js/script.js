btnprs('bmenus','wmenu','collapse');
btnprs('binfo','wnav','collapse');

btnprs('map-market','map-modal','u-visible');
btnprs('map-close','map-modal','u-visible');

tabs('map-market','map-tabs','u-visible','tab-close');

function btnprs(press, object, change){
    const $elemento = document.getElementById(press);
    if($elemento){
    	$elemento.addEventListener('click', (event)=>{
	        document.getElementById(object).classList.toggle(change); 
	    });
    }
};

function tabs(press, object, change, elementSee) {
	let clic = 0;
	const $elemento = document.getElementById(press);
	if($elemento){
		$elemento.addEventListener('click', (event)=>{
			if(clic === 0){
				document.getElementById(object).classList.add(change);
				clic = 1;
			}else{
				const $element2 = document.getElementById(elementSee);
				$element2.addEventListener('click', (event)=>{
			        document.getElementById(object).classList.remove(change); 
			        clic = 0;
			    });
			}
		});
	}
}